SUMMARY="The OpenGL Extension Wrangler Library"
DESCRIPTION="
The OpenGL Extension Wrangler Library (GLEW) is a cross-platform open-source \
C/C++ extension loading library. GLEW provides efficient run-time mechanisms \
for determining which OpenGL extensions are supported on the target platform.
"
LICENSE="BSD (3-clause)"
COPYRIGHT="
	2007 The Kronos Group Inc.
	2002-2007 Milan Ikits
	2002-2007 Marcelo E. Magallon
	2002 Lev Povalahev
	"
HOMEPAGE="http://glew.sourceforge.net"
SRC_URI="http://downloads.sourceforge.net/project/glew/glew/${portVersion}/glew-${portVersion}.tgz"
CHECKSUM_SHA256="99c41320b63f6860869b5fb9af9a1854b15582796c64ee3dfd7096dc0c89f307"
REVISION="5"

ARCHITECTURES="x86 ?x86_gcc2 ?x86_64"
SECONDARY_ARCHITECTURES="?x86_gcc2 ?x86"

PATCHES="glew-1.10.0.patchset"

PROVIDES="
	glew$secondaryArchSuffix = $portVersion
	lib:libGLEW$secondaryArchSuffix = $portVersion
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libGL$secondaryArchSuffix
	"
BUILD_REQUIRES="
	devel:libGL$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:gcc$secondaryArchSuffix
	cmd:g++$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	"

BUILD()
{
	make $jobArgs GLEW_DEST=$prefix BINDIR=$binDir LIBDIR=$libDir INCDIR=$includeDir/GL
}

INSTALL()
{
	make install GLEW_DEST=$prefix BINDIR=$binDir LIBDIR=$libDir INCDIR=$includeDir/GL
	make install.bin GLEW_DEST=$prefix BINDIR=$binDir LIBDIR=$libDir INCDIR=$includeDir/GL

	prepareInstalledDevelLib libGLEW
	fixPkgconfig

	packageEntries devel $developDir
	packageEntries util $binDir
}

# ----- devel package -------------------------------------
PROVIDES_devel="
	glew${secondaryArchSuffix}_devel = $portVersion
	devel:libGLEW$secondaryArchSuffix = $portVersion
	"

REQUIRES_devel="
	glew$secondaryArchSuffix == $portVersion base
	"

# ----- util package --------------------------------------
PROVIDES_util="
	glew${secondaryArchSuffix}_util = $portVersion
	cmd:glewinfo$secondaryArchSuffix
	cmd:visualinfo$secondaryArchSuffix
	"

REQUIRES_util="
	glew$secondaryArchSuffix == $portVersion base
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libGL$secondaryArchSuffix
	lib:libGLEW$secondaryArchSuffix
	lib:libstdc++$secondaryArchSuffix
	"
