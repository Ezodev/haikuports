SUMMARY="Native file sharing and chat application" 
DESCRIPTION="BeShare is a BeOS/Haiku-native file sharing program. \
It operates similarly to other such programs (Napster, Hotline, etc) \
but with a few advantages:

- BeShare allows you to upload or download any type \
of file, not just mp3.
- When browsing files, you can see their attributes \
(similar to Be's Tracker view)
- Your query is always \"live\", so if new files that \
match your query become available, you will see them \
added to the results list right away (you won't have to \
click \"refresh\" or anything first)
- BeShare includes a rudimentary chat mechanism for you social types.
- BeShare supports any number of simultaneous uploads and downloads \
in parallel, but will serialize simultaneous file requests to any \
given host for efficiency.
"
COPYRIGHT="1999-2012 Jeremy Friesner"
LICENSE="
	Public Domain
	libsanta
	"
HOMEPAGE="https://github.com/HaikuArchives/BeShare" 
SRC_URI="git+https://github.com/HaikuArchives/BeShare.git#29bf5a00162aba759ddae5016129a246414dbb8c"
SRC_URI_2="https://public.msli.com/lcs/muscle/muscle6.05.zip"
REVISION="1"
#CHECKSUM_SHA256=""
CHECKSUM_SHA256_2="59605af9ba140ab2070f93e8efdd96646891b040eea5c970caea4c5aaf2845a6"


ARCHITECTURES="x86 ?x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	beshare$secondaryArchSuffix = $portVersion
	app:BeShare = $portVersion
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	#lib:libz
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	#devel:libz$secondaryArchSuffix
	#devel:muscle$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	cmd:mkdepend
	makefile_engine
	"

PATCHES="beshare-4.patchset"
PATCHES_2="muscle-6.05.patchset"

BUILD()
{
	ln -s ../../sources-2/muscle muscle
	cd source
	make
	#LOCAL_INCLUDE_PATHS=$includeDir/muscle
}

INSTALL()
{
	mkdir -p $appsDir/BeShare
	cp source/objects.*/BeShare $appsDir/BeShare
	cp docs/*.txt $appsDir/BeShare
	addAppDeskbarSymlink $appsDir/BeShare/BeShare
}
